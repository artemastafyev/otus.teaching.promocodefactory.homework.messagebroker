﻿using System;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.Administration.Core.Services
{
    public interface IAppliedPromocodesService
    {
        Task UpdateAppliedPromocodesAsync(Guid id);
    }
}
