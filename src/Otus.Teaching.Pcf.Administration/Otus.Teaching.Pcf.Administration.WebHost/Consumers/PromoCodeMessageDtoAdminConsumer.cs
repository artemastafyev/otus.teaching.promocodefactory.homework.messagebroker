﻿using MassTransit;
using Otus.Teaching.Pcf.Administration.Core.Services;
using Otus.Teaching.Pcf.Contracts;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.Administration.WebHost.Consumers
{
    public class PromoCodeMessageDtoAdminConsumer : IConsumer<IPromoCodeMessageDto>
    {
        private readonly IAppliedPromocodesService _appliedPromocodesService;

        public PromoCodeMessageDtoAdminConsumer(IAppliedPromocodesService appliedPromocodesService)
        {
            _appliedPromocodesService = appliedPromocodesService;
        }

        public async Task Consume(ConsumeContext<IPromoCodeMessageDto> context)
        {
            var model = context.Message;

            if (model.PartnerManagerId.HasValue)
            {
                await _appliedPromocodesService.UpdateAppliedPromocodesAsync(model.PartnerManagerId.Value);
            }
        }
    }
}
