﻿using MassTransit;
using Otus.Teaching.Pcf.Contracts;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Services;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.GivingToCustomer.WebHost.Consumers
{
    public class PromoCodeMessageGtCConsumer :
        IConsumer<IPromoCodeMessageDto>
    {
        private readonly IGivePromoCodeService _givePromoCodeService;

        public PromoCodeMessageGtCConsumer(IGivePromoCodeService givePromoCodeService)
        {
            _givePromoCodeService = givePromoCodeService;
        }

        public async Task Consume(ConsumeContext<IPromoCodeMessageDto> context)
        {
            var model = context.Message;

            await _givePromoCodeService.GivePromoCodesToCustomersWithPreferenceAsync(model);
        }
    }
}

