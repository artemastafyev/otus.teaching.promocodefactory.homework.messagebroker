﻿using Otus.Teaching.Pcf.Contracts;
using System;

namespace Otus.Teaching.Pcf.GivingToCustomer.WebHost.Models
{
    public class GivePromoCodeRequest : IPromoCodeMessageDto
    {
        public Guid PartnerId { get; set; }

        public string BeginDate { get; set; }
        
        public string EndDate { get; set; }

        public Guid PreferenceId { get; set; }

        public string PromoCode { get; set; }

        public string ServiceInfo { get; set; }

        public Guid PromoCodeId { get; set; }

        public Guid? PartnerManagerId { get; set; }
    }
}