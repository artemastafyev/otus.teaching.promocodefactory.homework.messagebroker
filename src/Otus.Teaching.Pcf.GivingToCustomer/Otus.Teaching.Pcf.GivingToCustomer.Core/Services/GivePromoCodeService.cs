﻿using Otus.Teaching.Pcf.Contracts;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;
using Otus.Teaching.Pcf.GivingToCustomer.WebHost.Mappers;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.GivingToCustomer.Core.Services
{
    public interface IGivePromoCodeService
    {
        Task GivePromoCodesToCustomersWithPreferenceAsync(IPromoCodeMessageDto model);
    }

    public class GivePromoCodeService : IGivePromoCodeService
    {
        private readonly IRepository<PromoCode> _promoCodesRepository;
        private readonly IRepository<Preference> _preferencesRepository;
        private readonly IRepository<Customer> _customersRepository;

        public GivePromoCodeService(IRepository<PromoCode> promoCodesRepository,
                                    IRepository<Preference> preferencesRepository,
                                    IRepository<Customer> customersRepository)
        {
            _promoCodesRepository = promoCodesRepository;
            _preferencesRepository = preferencesRepository;
            _customersRepository = customersRepository;
        }

        public async Task GivePromoCodesToCustomersWithPreferenceAsync(IPromoCodeMessageDto model)
        {
            var preference = await _preferencesRepository.GetByIdAsync(model.PreferenceId);

            if (preference == null)
            {
                throw new NullReferenceException("Предпочтение не найдено");
            }

            //  Получаем клиентов с этим предпочтением:
            var customers = await _customersRepository
                .GetWhere(d => d.Preferences.Any(x =>
                    x.Preference.Id == preference.Id));

            PromoCode promoCode = PromoCodeMapper.MapFromModel(model, preference, customers);

            await _promoCodesRepository.AddAsync(promoCode);
        }
    }
}
